class Post < ActiveRecord::Base
  belongs_to :user

  validates :title, presence: true
  validates :hat, presence: true
  validates :content_post, presence: true

  mount_uploader :thumbnail, ThumbnailUploader
end
