class PostsController < ApplicationController
   before_action :load_photo, only: [:show, :edit, :update, :destroy]
   before_action :required, only:[:edit, :update, :destroy]
  def index
    @posts = Post.all.order(created_at: :desc)
    @users = User.all
  end

  def show
    @user = User.find(@post.user_id)
  end

  def update
      if @post.update_attributes authorized_attributes
          redirect_to @post
      else
          render "edit"
      end
  end

  def new
    @post = Post.new
  end

  def destroy
        Post.destroy(@post.id)
        redirect_to :posts
    end

  def create
        @post = Post.new authorized_attributes
        @post.user = current_user
        if @post.save
            redirect_to @post
        else
            render "new"
        end
    end

    def load_photo
        @post = Post.find params[:id]
    end

    def required
      if @post.user_id.to_f != current_user
        redirect_to('/')
      end
    end

    def authorized_attributes
      authorized_attributes = params.require(:post).permit(
          :title,
          :content_post,
          :hat,
          :thumbnail
      )
  end
end
