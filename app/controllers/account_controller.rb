class AccountController < ApplicationController
  def show
    @user = User.find params[:id]
    @posts = Post.where(@user.id)
  end
end
