Rails.application.routes.draw do
  devise_for :users
  resources :posts
  root 'posts#index'
  get "/users/:id", to: "account#show", as: "user_account"
end
